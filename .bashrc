## /data/data/com.termux/files/usr/bin:data/data/com.termux/files/home/bin
#if [ "$UID" -ne 0 ]; then printf '%s\n' "root needed" ; exit 1 ; fi ##[ -z "${1}" ] || printf '%s\n' "! ${1}" ##[ $# -gt 0 ] || usage ""
case $- in *i*) ;; *) return ;; esac
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

### VARIABLES ###
#export TZ=Singapore
#export LANG=en_US.UTF-8
#export LC_TYPE=en_US.UTF-8
#export PAGER="less -iR --use-color -Dd+Gg -Du+b"  #export MANPAGER="less -iR --use-color -Dd+Gg -Du+b"   # Colored man
#PS1="\[\033[38;5;9m\]\w\[$(tput sgr0)\]\[\033[38;5;7m\] \[$(tput sgr0)\]"
#if [ -d "${HOME}/.go" ]; then export GOPATH="${HOME}/.go"; export PATH="$PATH:$GOPATH/bin"; fi
#if [ -d "$HOME/node_modules/.bin" ]; then export PATH="$HOME/node_modules/.bin:$PATH"; fi
#export TERM=xterm-256color
export BROWSER=w3m
export EDITOR="vim"
export VISUAL="vim"
export SUDO_EDITOR="vim"
export PS1="\[\e[32m\]\w \$\[\e[m\] "
export PROMPT_COMMAND="${PROMPT_COMMAND}${PROMPT_COMMAND:+;}history -a; history -n" #sync history and bash_history https://unix.stackexchange.com/a/131507
LESS="-iR"; export LESS ##if busybox less -MI

#### HISTORY settings ################
HISTFILE="$HOME/.bash_history"
HISTTIMEFORMAT="%F %T "   #hist time format yyyymmdd HH:MM:SS
HISTCONTROL=ignoreboth    # don't put duplicate lines or lines starting with space in the history
HISTSIZE=50000
HISTFILESIZE=50000
shopt -s histappend histverify checkwinsize extglob cmdhist #autocd cdspell
bind 'TAB:menu-complete'    # cycle through all matches with 'TAB' key
stty stop ""                # avoid ctrl-s freeze window

# enable color support of ls, grep and ip, also add handy aliases
if [ -x /usr/bin/dircolors ]; then
( test -r "$HOME"/.dircolors && eval "$(dircolors -b "$HOME"/.dircolors)" ) || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias diff='diff --color=auto'
    alias ip='ip -color'
fi

# command-not-found will give a package suggestions.
if [ -x /data/data/com.termux/files/usr/libexec/termux/command-not-found ]; then
    command_not_found_handle() { /data/data/com.termux/files/usr/libexec/termux/command-not-found "$1" ;}
fi
#### ALIASES #########################
### termux/sh/bash/ash/ksh SHELL specific
#t11() { termux-x11 :0 -xstartup "dbus-launch --exit-with-session xfce4-session" ;} #-dpi 120 to resize font
alias tusr='cd /data/data/com.termux/files/usr ; ls -FAh --group-directories-first'
alias tetc='cd /data/data/com.termux/files/usr/etc ; ls -FAh --group-directories-first'
alias pst=termux-clipboard-get # paste
alias cpy=termux-clipboard-set # copy
alias open=termux-open # open with external app
alias wla='termux-wake-lock'
alias wlr='termux-wake-unlock'
tv() { termux-volume music "${*}" >/dev/null 2>&1 ; echo curr-"$(termux-volume | jq ".[] | select(.stream==\"music\").volume" )" "##val 0-15" ;}
tvd() { tvol="$(termux-volume | jq ".[] | select(.stream==\"music\").volume" )"; termux-volume music "$((tvol-2))"; echo "$((tvol-2))" ;}
tvu() { tvol="$(termux-volume | jq ".[] | select(.stream==\"music\").volume" )"; termux-volume music "$((tvol+1))"; echo "$((tvol+1))" ;}
tb() { termux-brightness "${*}"; echo "##val 0-255 enable termux write.setting permission 1st" ;}
alias u='sudo apt update' 
alias uu='sudo apt update ; sudo apt upgrade -y'
#remove 2 last fields : sed s/-[^-]*-[^-]*$//g)" or awk -F- 'BEGIN{OFS="-"}NF=(NF-2)'
s() { sel="$(apt-cache pkgnames | fzf --cycle --height=50% -q "${*}" )" && sudo apt install "$sel" && unset sel ;}
as() { sel="$(apt-cache pkgnames | fzf --cycle --height=50% -q "${*}" )" && apt show "$sel" && unset sel ;}
r() { sel="$(dpkg -l | awk '{print $2}' | fzf --cycle --height=50% -q "${*}" )" && sudo apt remove "$sel" && unset sel ;}
alias i='sudo apt install'
#i='nix-env -iA'  r='nix-env -e'  u='nix-env -u' l='nix-env -q' nix-env --list-generations/--switch-generation/--delete-generations?  nix-env --rollback

alias brc='vim "$HOME"/.bashrc ; . "$HOME"/.bashrc'
alias bashrc='mv -v "$HOME/.bashrc" "$HOME/.bashrc.$(date +%M%S)" && wget -cqO- https://gitlab.com/shaddixzack/ter/-/raw/main/.bashrc > "$HOME"/.bashrc ; . "$HOME"/.bashrc'
#alias bashrc='mv -v "$HOME/.bashrc" "$HOME/.bashrc.$(date +%M%S)" && curl -C -L https://gitlab.com/shaddixzack/ter/-/raw/main/.bashrc > "$HOME"/.bashrc ; . "$HOME"/.bashrc'
alias 800='ssh -vX pa@192.168.1.100 -t "tmux attach -t 800 -d || tmux new -s 800"'
alias g1='ssh -vX pa@192.168.1.101 -t "tmux attach -t g1 -d || tmux new -s g1"'
alias svd='ssh -vX pa@192.168.1.102 -t "tmux attach -t svd -d || tmux new -s svd"'
alias 390='ssh -vX pa@192.168.1.103 -t "tmux attach -t 390 -d || tmux new -s 390"'
alias v14='ssh -vX pa@192.168.1.104 -t "tmux attach -t v14 -d || tmux new -s v14"'
alias hpdf='ssh -vX pa@192.168.1.105 -t "tmux attach -t hpdf -d || tmux new -s hpdf"'
ssht() { ssh -vX "${@}" -t "tmux attach -t $(uname -n) -d || tmux new -s ssh" ;}
sshk() { ssh-keygen -N "${*}" -t ed25519 -f "$HOME/.ssh/id_ed25519" ;}
sshc() { cat "$HOME/.ssh/id_ed25519.pub" | ssh "${*}" 'mkdir ~/.ssh; cat >> "$HOME/.ssh/authorized_keys"' ;}
ssha() { eval "$(ssh-agent)"; ssh-add "$HOME/.ssh/id_ed25519" ;}
sshdl(){ scp -CT "${*}" "${SSH_CLIENT%% *}":"$HOME/dls" ;}
rmp3() { rsync -aP --delete-delay -e ssh pa@192.168.1.100:"${HOME}"/mp3/ "${HOME}"/mp3/ ;}
#mo() { mosh "${@}" -- tmux attach -t 0 -d ;} #e.g mosh --ssh="ssh -p 822" ed8@vm-ed -- tmux attach -t irc. ##$HOSTNAME/uname -n/hostname -f

### CONTROL SHORTCUTS ###
alias cmus='cmus || tmux attach -t "$HOSTNAME" -d'
alias cmn='cmus-remote -n'
alias cmp='cmus-remote -r'
alias cmr='cmus-remote -c -l && cmus-remote -l "${HOME}"/mp3'
alias cm+='cmus-remote -C "vol -3%"'
alias cm-='cmus-remote -C "vol +2%"'
#cmus --listen 0.0.0.0  ##cmus-remote --server 0.0.0.0 --passwd yourpassword -Q
#batt() { printf "%.3s-%s%%\n" "$(cat /sys/class/power_supply/BAT0/status)" "$(cat /sys/class/power_supply/BAT0/capacity)" ;}
webc() { v4l2-ctl --set-fmt-video=width=200 ; mpv av://v4l2:/dev/video0 --vf=hflip --profile=low-latency --untimed ;} #or mpv /dev/video0
#vu() { ( [ -n "${*}" ] && amixer sset Master "${*}"% unmute ) || amixer sset Master 2%+ unmute ;}
#vd() { ( [ -n "${*}" ] && amixer sset Master "${*}"% ) || amixer sset Master 3%- ;}
#vm() { amixer sset Master 0%; amixer sset Master toggle ;}
#vh() { amixer sset 'Speaker' 0% mute && amixer sset 'Line Out' 0% mute && amixer sset 'Headphone' 85% unmute ;}
#vl() { amixer sset 'Speaker' 0% mute && amixer sset 'Headphone' 0% mute && amixer sset 'Line Out' 85% unmute ;}
#vs() { amixer sset 'Line Out' 0% mute && amixer sset 'Headphone' 0% mute && amixer sset 'Speaker' 85% unmute ;}
#mm() { amixer sset Capture nocap 0% ;}
#mu() { ( [ -n "${*}" ] && amixer sset Capture cap "${*}"% ) || amixer sset Capture cap 40% ;}
#bu() { ( [ -n "${*}" ] && brightnessctl set "${*}"% ) || brightnessctl set 2%+ ;}
#bd() { ( [ -n "${*}" ] && brightnessctl set "${*}"% ) || brightnessctl set 3%- ;}
#ruw() { rfkill unblock wlan ;}
#rub() { rfkill unblock bluetooth ;}
#run() { rfkill unblock nfc ;}
#rba() { rfkill block all ;}
#rbb() { rfkill block bluetooth ;}
#rbw() { rfkill block wifi ;}
#rbn() { rfkill block nfc ;}
#off() { poweroff || shutdown -Ph now ;}
#on() { reboot || shutdown -r ;}
#lo() { kill -9 -1 ;} ##C+A+Backspace / sudo pkill -u "${USER}" / loginctl kill-user "$(whoami)"

### COMMON CMD ###
alias ..='cd .. ; ls -FAh --group-directories-first'
alias ...='cd ../.. ; ls -FAh --group-directories-first'
alias ....='cd ../../.. ; ls -FAh --group-directories-first'
#alias l='ls -FAh --group-directories-first'
#alias ll='ls -lFAh --group-directories-first'
l() { case "${*}" in "") ls -FAh --group-directories-first;; *) ls -FAh --group-directories-first "${*}" 2> /dev/null || ( g="$(find "${PWD}" | fzf --cycle --height=50% -q "${*}" -1 -0)" && ls -FAh --group-directories-first "${g}" ) ;; esac ;}
ll() { case "${*}" in "") ls -lFAh --group-directories-first;; *) ls -lFAh --group-directories-first "${*}" 2> /dev/null || ( g="$(find "${PWD}" | fzf --cycle --height=50% -q "${*}" -1 -0)" && ls -lFAh --group-directories-first "${g}" ) ;; esac ;}
alias lss='ls -lFAhS --group-directories-first | less -Mi'
alias lll='ls -FAh --group-directories-first | less -Mi'
alias lsn='ls -FAh --group-directories-first | cat -n'
alias lt='ls -lAFhtr --group-directories-first | less -Mi' # recent last
alias mkdir='mkdir -p -v'
f() { ( [ -n "${*}" ] && find "${2:-.}" -iname "*${1}*" ) || echo "usage: f 1~VALUE 2~DIR" ;}
pk() { sel="$(ps ax | fzf --algo=v1 --cycle --height=50% | awk '{print $1}')" && kill -9 "$sel" && unset sel ;}
alias cp='rsync -aP --info=progress2'
alias mv='mv -v'
alias rm='rm -iv'
alias cs='printf "\033c"'
alias q='exit'
alias sudo='sudo '
alias vim='sudo -E vim' #retain vim env and read .vimrc, https://stackoverflow.com/a/23523119
alias smci='sudo make clean install'
alias dmci='doas make clean install'
alias make='make -j$(nproc)'
#tree() { find . -print | sed -e 's;[^/]*/;|__;g;s;__|; |;g' ;}
alias tmux='tmux -2'
alias gc='git clone --depth 1'
alias hh='hstr'
pi() { ping -Ac 3 192.168.1.1; echo; ping -Ac 3 9.9.9.9; echo; ping -Ac 3 quad9.net ;}
cpb() { fzf -m --cycle --height=50% < "${HOME}"/.bashrc | xsel -ipsb ;}
cph() { history | fzf -m --cycle --height=50% | sed 's/ *[0-9]* *//' | tr -d '\n' | xsel -ipsb ;} #bind '"\C-F":"fh\n"'	# fzf history
cpl() { sel="$(find "${PWD}" -type f | fzf --cycle --height=50% -q "${*}" -1 -0)" && fzf -m --cycle --height=50% < "$sel" | xsel -ipsb && unset sel ;}
cpy() { fzf -m --cycle --height=50% | tr -d '\n' | xsel -ipsb ;} ##xclip -sel c
pst() { xsel -opsb ;} ##xclip -o
#yank() { yank-cli -- xsel -ipsb ;}
#setxkbmap -option caps:swapescape ##xmodmap -e "keycode 66 = Escape NoSymbol Escape"     
#scoff() { setterm --blank 1 --powerdown 2 ;} ##tty
#scoff() { TERM=linux; setterm --blank 1 --powerdown 2 >/dev/tty1 </dev/tty1 ;} ##tty
scoff() { sleep 1 && xset -display "$DISPLAY" dpms force off ;} ##X11
#scoff() { setterm --term linux --blank < /dev/tty1 ;} ##ssh
#add to /etc/default/grub 4 permanent; GRUB_CMDLINE_LINUX_DEFAULT="quiet consoleblank=60"
#sudo dpkg-reconfigure console-setup  # Suggested Terminus, 16x32
#sudo systemctl restart console-setup.service  # To apply immediately to all TTYs

### APPLICATIONS ###
oll() { ollama serve& sleep 1 && ollama run llama3.1 ;}
wpp() { sel="$(find "$HOME"/pix/* | shuf -n 1)" && xwallpaper --maximize "$sel" && unset sel ;}
xwp() { sel="$(sxiv -tfpo "$HOME"/pix/*)" && xwallpaper --maximize "$sel" && unset sel ;}
#tsp mpv --fs --screen=1 $(xclip -o) 1>/dev/null
dlnaserv() { ssh -vv -X pa@192.168.1.100 -t "tmux attach -t 800 -d || tmux new -s 800 -d; tmux send-keys -t 800 dlna Enter" ;}
dlnabrow() { dlna-browser "${*}" | xargs mpv --fs --speed=1.1 --force-seekable=yes ;}
#alias ytfzf='ytfzf -t -T chafa'
ym() { yt-dlp -f "(mp4)[height<=480]" -o - "$(xclip -o)" | mpv - --fs --force-seekable=yes --no-cache --speed=1.35 ;}
yd() { yt-dlp -f "(mp4)[height<480]" -o "%(title)s.%(ext)s" --restrict-filenames --write-sub --write-auto-sub --sub-lang "en.*" --downloader=aria2c --downloader "dash,m3u8:native" -- "${*}" ;}
ya() { yt-dlp -x -f bestaudio -o "%(title)s.%(ext)s" --restrict-filenames --downloader=aria2c -- "${*}" ;}
yp() { yt-dlp -f best -o "%(playlist_index)s.%(title)s.%(ext)s" --downloader=aria2c -- "${*}" ;}
yt() { yt-dlp -f best -o "%(title)s.%(ext)s" --restrict-filenames --downloader=aria2c -- "${*}" ;}
abc() { yt-dlp -f "(mp4)[height<=480]" -o - https://www.youtube.com/watch?v=vOTiJkg1voo | mpv - --fs --no-cache ;}
abc1() { yt-dlp -f "(mp4)[height<=480]" -o - https://vid.puffyan.us/watch?v=vOTiJkg1voo | mpv - --fs --no-cache ;}
alj() { yt-dlp -f "(mp4)[height<=480]" -o - https://www.youtube.com/watch?v=gCNeDWCI0vo | mpv - --fs --no-cache ;}
alj1() { yt-dlp -f "(mp4)[height<=480]" -o - https://vid.puffyan.us/watch?v=gCNeDWCI0vo | mpv - --fs --no-cache ;}
cna() { yt-dlp -f "(mp4)[height<=480]" -o - https://www.youtube.com/watch?v=XWq5kBlakcQ | mpv - --fs --no-cache ;}
cna1() { yt-dlp -f "(mp4)[height<=480]" -o - https://vid.puffyan.us/watch?v=XWq5kBlakcQ | mpv - --fs --no-cache ;}
trt() { yt-dlp -f "(mp4)[height<=480]" -o - https://www.youtube.com/watch?v=5VF4aor94gw | mpv - --fs --no-cache ;}
trt1() { yt-dlp -f "(mp4)[height<=480]" -o - https://vid.puffyan.us/watch?v=5VF4aor94gw | mpv - --fs --no-cache ;}
wion() { yt-dlp -f "(mp4)[height<=480]" -o - https://www.youtube.com/watch?v=gadjsB5BkK4 | mpv - --fs --no-cache ;}
wion1() { yt-dlp -f "(mp4)[height<=480]" -o - https://vid.puffyan.us/watch?v=gadjsB5BkK4 | mpv - --fs --no-cache ;}
nhk() { yt-dlp -f "(mp4)[height<=480]" -o - https://www.youtube.com/watch?v=f0lYkdA-Gtw | mpv - --fs --no-cache ;}
nhk1() { yt-dlp -f "(mp4)[height<=480]" -o - https://vid.puffyan.us/watch?v=f0lYkdA-Gtw | mpv - --fs --no-cache ;}
euro() { yt-dlp -f "(mp4)[height<=480]" -o - https://www.youtube.com/watch?v=pykpO5kQJ98 | mpv - --fs --no-cache ;}
euro1() { yt-dlp -f "(mp4)[height<=480]" -o - https://vid.puffyan.us/watch?v=pykpO5kQJ98 | mpv - --fs --no-cache ;}
#mywan() { wget -cqO- ipleak.net/json/ || wget -cqO- icanhazip.com || wget -cqO- ifconfig.me || wget -cqO- l2.io/ip || wget -cqO- ip.tyk.nu || wget -cqO- eth0.me ;}
#mylan() { ip r | grep -oE '([0-9]{1,3}\.){3}[0-9]{1,3}' | tail -n1 || ifconfig | awk '/cast/ {printf $2}' ;}
#mytime() { wget -cqO- "http://worldtimeapi.org/api/timezone/Singapore.txt" ;}
wsp() { k="$(ip r | grep -oE '([0-9]{1,3}\.){3}[0-9]{1,3}' | tail -n1)" && qrencode -t utf8 http://"${k}":50080 && echo http://"${k}":50080 && python3 -m http.server 50080 ;}
#webfsd -F -p 50080 / miniserve -qu -p 50080 / ruby -run -ehttpd . -p50080 / php -S localhost:50080 -t .
#wsh() { k="$(ip r | grep -oE '([0-9]{1,3}\.){3}[0-9]{1,3}' | tail -n1)" && qrencode -t utf8 http://"$k":50080 &&  busybox httpd -p 50080 -f -v ;} #index.html 
#busybox tcpsvd -vE 0.0.0.0 1024 busybox ftpd -w ~
#alias ani='ani-cli -q 480p -d'
alias nb='newsboat'
alias mb='mbsync -a'
alias mutt='neomutt'
alias aria2c='aria2c -x7 -c'
alias df='df -ha'
alias du='du -had 1'
#alias free='free -mht'
alias ps='ps auxf'
alias ht='htop'
alias del='shred -uzn3'
alias ports='sudo netstat -tulanp'

#qemu-system-x86_64 -enable-kvm -boot menu=on -drive file=/home/john/VMs/win10.qcow2,format=qcow2 -m 2G -smp 2 -cpu host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time -audiodev pa,id=snd0 -device ich9-intel-hda -device hda-output,audiodev=snd0 -vga virtio -display sdl,gl=on
#qemu-system-x86_64 -enable-kvm -m 2048 -nic user,model=virtio -drive file=alpine.qcow2,media=disk,if=virtio -cdrom alpine-standard-3.8.0-x86_64.iso -sdl/nographic console=ttyS0 
#qemu-img create -f qcow2 alpine.qcow2 5G
#alias wttr='curl wttr.in/beranang' # change the place to yours
alias urls='mv -v "$HOME/.newsboat/urls" "$HOME/.newsboat/urls.$(date +%M%S)" && wget -cqO- https://gitlab.com/shaddixzack/dot/-/raw/main/urls > "$HOME/.newsboat/urls"'
#alias urls='mv -v "$HOME/.newsboat/urls" "$HOME/.newsboat/urls.$(date +%M%S)"  && curl -C -L https://gitlab.com/shaddixzack/dot/-/raw/main/urls -o "$HOME/.newsboat/urls"'
alias stcli='curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python3 -'
alias lynx='lynx --useragent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.79 Safari/537.1 Lynx"'
#alias met-se='lynx -dump -width 150 "https://www.met.gov.my/en/forecast/weather/town/Tn093" | grep -i "location\ \:" -A 25'
#alias met-ni='lynx -dump -width 150 "https://www.met.gov.my/en/forecast/weather/town/Tn096" | grep -i "location\ \:" -A 25'
#alias met-pd='lynx -dump -width 150 "https://www.met.gov.my/en/forecast/marine/waters/Co010" | grep -i "location\ \:" -A 25'
#tide_pk() { lynx -width 150 -dump https://wisuki.com/tide/459/port-klang | grep -A3 -i $(date +"%d/%m/%+4Y") ;}
tide_tp() { lynx -width 150 -dump https://www.tide-forecast.com/locations/Tumpat/tides/latest | grep -A6 -i "Tumpat: $(date +"%A %d %B %+4Y")" ;}
tide_pd() { lynx -width 150 -dump https://www.tide-forecast.com/locations/Port-Dickson-Malaysia/tides/latest | grep -A6 -i "Port Dickson: $(date +"%A %d %B %+4Y")" ;}
tide_pk() { lynx -width 150 -dump https://www.tide-forecast.com/locations/Pelabuhan-Kelang-Malaysia/tides/latest | grep -A6 -i "Klang: $(date +"%A %d %B %+4Y")" ;}
m2u() { librewolf --private-window https://www.maybank2u.com.my ;}
hbmy() { librewolf --private-window https://www.hsbc.com.my ;}

#alias scrh='scrcpy --lock-video-orientation=3'
#alias scrhi='scrcpy --lock-video-orientation=1'
#alias scrv='scrcpy --lock-video-orientation=0'
#alias scrvi='scrcpy --lock-video-orientation=2'

#### FUNCTIONS ######################
#dec() { gpg --batch --output "${1}".dec --passphrase "${2}" --decrypt "${1}" ;}
#enc() { gpg --batch --output "${1}".gpg --passphrase "${2}" --symmetric "${1}" ;}
#isowrite() { cdrecord dev=1,0,0 fs=32M driveropts=burnfree speed=120 gracetime=1 -v -dao -eject -pad -data "${*}";}
#burn() { mkisofs -J -r -o /tmp/t.iso "${*}"; sleep 1 ; cdrecord dev=1,0,0 driveropts=burnfree speed=16 -eject /tmp/t.iso ;}
#burn() { mkisofs -J -r "${*}" | cdrecord dev=ATAPI:0,1,0 -dummy speed=16 -v -multi -pad -data - ;} #-dummy only for simulation
##driveropts=burnfree, buffer protection, type cdrecord dev=<device> driveropts=help -checkdrive into a terminal. You should see "burnfree"
#multi-sess-burn () { mkisofs -J -r -C $(cdrecord dev=ATAPI:0,1,0 -msinfo) -M ATAPI:0,1,0 -o output1.iso "${*}" ;}
#ripcd() { cdparanoia -Q -d /dev/hdc ;}
#scanbus() { cdrecord -atip ; cdrecord -scanbus; cdrecord -dev=ATAPI -scanbus ;}
#mount -t iso9660 /dev/sr0 /media #'-o loop' for .iso image file
#mount -t ntfs-3g/vfat /dev/sdc1 /mnt #mount -t nfs -o vers=4  192.168.0.121:/mydata /mnt
#tmhist() { tmux set-option -g history-limit "${1}" \; new-session -s "${2}" \; set-option -g history-limit 5000 ;}
#seq() { i=1 ;while [ "${i}" -ne 11 ] ;do printf "${i}\n" ;i=$(( "${i}" + 1 )) ;done ;}
#lsext() { find . -type f -iname '*.'${1}'' -exec ls -FAh --group-directories-first {} \; ;}
#batchexec() { find . -type f -iname '*.'${1}'' -exec ${@:2}  {} \; ;}
#rpass() { cat /dev/urandom | tr -cd '[:graph:]' | head -c ${1:-12} ;}
#log() { printf "[$(date +%Y%m%d\.%H%M%S)]"${*}"\n" ;}
#htmldecode() { : "${*//+/ }"; printf "${_//&#x/\x}" | tr -d\; ;}
#urldecode() { : "${*//+/ }"; printf "${_//%/\\x}" ;}
#expandurl() { curl -sIL $1 | awk '/^Location/ {print $2}' ;}
#calc() { printf '%s\n' "scale=${2:-3};${1}" | bc ;} #1=operation 2=fractional/decimal point
#wi() { [ $# -ne 0 ] && printf '%s\n\n' "$(file "${@}")" "$(stat "${@}")" "$(hexdump -C "${@}" | head)" ;} #type strings ldd ltrace strace readelf objdump nm gdb radare2 ghidra
#csv2json() { python3 -c "import csv,json,sys;print(json.dumps(list(csv.reader(open(sys.argv[1])))))" "${*}" ;}
#bin2dec() { printf '%s\n' "obase=10; ibase=2; $1" | bc ;} 
#bin2hex() { printf '%s\n' "obase=16; ibase=2; $1" | bc ;} 
#dec2bin() { printf '%s\n' "obase=2; ibase=10; $1" | bc ;} 
#dec2hex() { printf '%s\n' "obase=16; ibase=10; $1" | bc ;} 
#hex2bin() { printf '%s\n' "obase=2; ibase=16; $1" | bc ;} 
#hex2dec() { printf '%s\n' "obase=10; ibase=16; $1" | bc ;}
#f2c() { printf '%s\n' "scale=${2:-1};(${1} - 32) * 5/9" | bc ;} #fahrenheit to celcius
#c2f() { printf '%s\n' "scale=${2:-1};(${1} * 9/5) - 32" | bc ;} #celcius to fahrenheit
#biggest() { du -k * | sort -nr | cut -f2 | head -20 | xargs -d "\n" du -sh ;}
#top10() { history | awk '{print $2}' | sort | uniq -c | sort -rn | head ;}
#beep() { printf '\a\n' ;}
#get_pid() { ps ef | grep "${1}" | grep -v grep | awk '{ print $2; }' ;}
#flattenSubfolders() { find . -type f -exec mv -n -- {} . \; ;}
#removeEmptySubdirs() { find . -depth -mindepth 1 -type d -empty -exec rmdir {} \; ;}
#any2WebMp4() { ffmpeg -i "${*}" -c:v h264 -c:a aac -strict -2 "${*}".mp4 ;}
#any2Webm() { ffmpeg -i "${*}" -c:v libvpx-vp9 -b:v 2M -pass 1 -c:a libopus -f webm /dev/null && ffmpeg -i "${*}" -c:v libvpx-vp9 -b:v 2M -pass 2 -c:a libopus "${*}".webm ;}
#wavToMp3() { ffmpeg -i "${*}" -c:a libmp3lame -qscale:a 2 "${*}".mp3 ;}
#mp3ToOgg() { for f in ./*.mp3; do ffmpeg -i "${f}" "${f}".ogg; done ;}
#to360p() { for f in ./*.mp4; do ffmpeg -i "${f}" -vf scale=-1:360 -c:v libx264 -crf 25 -preset slower -c:a copy -c:s copy "${f}".mkv ; unset -v "${f}" ;done ;}
#to480p() { sel="$(find . | fzf --cycle --height=50% -q "${*}" -1 -0)" && ffmpeg -i "${sel}" -vf scale=-1:480 -c:v libx264 -crf 25 -preset fast -c:a copy -c:s copy "${sel}".mkv && unset sel ;}
#m4a2mp3() { for sel in ./*.m4a; do ffmpeg -i "${sel}" -c:a libmp3lame -ab 320k "${sel%.m4a}.mp3"; unset sel ;done ;}
#vid2mp3() { for sel in ./*; do ffmpeg -i "${sel}" -vn -ar 44100 -ac 2 -ab 192 -f mp3 "${sel}".mp3 ; unset sel ;done ;}
#wma2ogg() { find -name '*wma' -exec ffmpeg -i {} -c:a vorbis -ab 128k {}.ogg \; ;}
#webm2ogg() { for sel in "${@}"; do ffmpeg -v warning -i "${sel}" -vn -c:a copy "${sel%.webm}.ogg" && rm "${sel}"; unset sel ;done ;}
#2worstmp3() { for sel in "${@}"; do ffmpeg -v warning -n -i "${sel}" -c:a libmp3lame -qscale:a 9 output.mp3 && mv -v output.mp3 "$sel-wq.mp3"; unset sel ;done;}
#2lqmp3() { for sel in "${@}"; do ffmpeg -v warning -n -i "${sel}" -c:a libmp3lame -qscale:a 7 output.mp3 && mv -v output.mp3 "$sel-lq.mp3"; unset sel ;done;}
#flv2mp4() { find . -name "*.flv" -type f -exec ffmpeg -n -i '{}' -c:a copy -c:v copy -copyts '{}.mp4' ; ;}
#flac2mp3() { for sel in ./*.flac; do ffmpeg -i "${sel}" -ab 320k "${sel%.*}.mp3"; unset sel ;done ;}
#shot() { import -frame -strip -quality 75 ""$HOME"/$(date +%s).png" ;}
#clock() { while printf "\033c"; do printf '%s\n' "===========" "$(date +%r)" "==========="; sleep 1; done ;}
#man() { env man "${@}" | sed s/−/-/g | ${MANPAGER:-${PAGER:-less}}; }
#webdl() { wget --random-wait -r -p -e robots=off -U mozilla "${*}" ;} ##download entire website
#rms() { find . -depth -exec sh -c 'for f do g=${f##*/}; mv -v "${f}" "${f%/*}/${g// /-}"; done;' _ {} + ;}
#rms() { awk -F'\.'$1' *' -v ext=".$1" '{ NF--;for(i=1;i<=NF;i++){gsub(" ","_",$i);printf "%s",$i ext (i<NF?" ":"\n")}}' ;}
#rms() { for sel in ./*" "*; do y=$(printf %sa "${sel}" | tr "[:space:]" "_") ; mv -v -- "${sel}" "${y%a}" ; unset sel ;done ;} #obligatory posix solution
rms() { for sel in ./*" "*; do ect=$(printf %sa "${sel}" | tr -cs "[:alnum:]" "_") ; mv -v -- "${sel}" "${ect%a}" ; unset sel ect;done ;} #obligatory posix solution
rmss() { sel="$(find "${PWD}" | fzf --cycle --height=50% -q "${*}")" && ect=$(printf '%s' "$sel" | tr -cs "[:alnum:]" "_") && mv -v "$sel" "$ect" && unset sel ect ;}
ffn() { sel="$(find "${PWD}" -type f | fzf --cycle --height=50% -q "${*}")" && ect=$(printf '%s' "${sel##*/}" | tr -cs "[:alnum:]" "_" | sed 's/\(.*\)_/\1\./') && mv -v "$sel" "$ect" && unset sel ect ;}
#srt() { for sel in ./*_en[._]* ./*_id.*; do ect=$(printf %sa "${sel}" | sed 's/_en_US//g;s/_en_GB//g;s/_en\./\./g;s/_id\./\./g') ; mv -v -- "${sel}" "${y%a}"; unset sel ect;done ;} #fix -en ext srt in newpipe
#srt1() { sel="$(fzf --cycle --prompt="SRT>")" && ect="$(fzf --cycle --prompt="MKV/MP4>")" && ion="${ect%.*}" && cp -v "$sel" "$ion".srt && unset sel ect ion ;}
#lower() { echo "${*}" | tr '[:upper:]' '[:lower:]' ;}
#upper() { echo "${*}" | tr '[:lower:]' '[:upper:]' ;}
dict() { curl "dict://dict.org/d:${1%%/}";}
bk() { cp -v "$1" "${1}"."$(date +%Y%m%d%H%M%S)" ; }
txt2qr() { xsel -opsb | qrencode -t UTF8 || xclip -o | qrencode -t UTF8 ;}
#cpr() { rsync -aPh --info=progress2 "$1" "$2" ;}
#mvr() { rsync -aPh --info=progress2 --remove-source-files "$1" "$2" ;} #careful of trailing slash!!
#cpp() { grep -oE '(https?|git|gopher|gemini|ftp|nfs|smb|imaps|smtps)://([[:graph:]]*\.)*([[:graph:]]*/)*[[:graph:]]*' /tmp/rd/w3m.tmp | uniq | fzf -m --cycle --height=50% | tr -d '\n' | xsel -ipsb ;} #for torr pix url
cpt() { grep -ioE '\w{40,}' /tmp/rd/w3m.tmp | cut -c 2- | uniq | tail -n 1 | xsel -ipsb ;} #for torr hash
w3mt() { mkdir -p /tmp/rd && sudo mount -t tmpfs rd -o size=100M,mode=777 /tmp/rd && script -c 'w3m -v' /tmp/rd/w3m.tmp && sudo umount /tmp/rd ;}
#ram() { mkdir -p /tmp/rd && sudo mount -t tmpfs rd -o size="${*}",mode=777 /tmp/rd && cd /tmp/rd ;}
mac() { sel="$(ls /sys/class/ieee80211/*/device/net/)" && rfkill block all && sleep 0.2 && sudo macchanger -r "${sel}" && sudo macchanger -r "${sel}" && sleep 0.2 && rfkill unblock wlan && unset sel ;}
#dos2unix() { perl -pie 's/\r//g' "${*}" ;} #sed -i 's/\r//' "${*}" #tr -d '\r' "${*}" #awk 'gsub(/\r/,"")' "${*}" #vim set ff=unix
#docx() { unzip -p "$1" word/document.xml | sed -e 's/<\/w:p>/\n /g' -e 's/<[^>]*>//g' | less -Mi ;}
#pdfi() { [ -n "${*}" ] || return; x="${*}" && n="$(printf '%s' "${x%%.*}")"; soffice --headless --convert-to jpg "${*}"; img2pdf -o "$n".pdf "$n".jpg ;}
fl() { grep -e "()\ {\ " -e "alias" --color=never "$HOME"/.bashrc | grep -i "${*}" | sort ;}

c() { case "${*}" in "") ls -FAh --group-directories-first;; *) cd "${*}" 2> /dev/null || sel="$(find "${PWD}" -type d | fzf --cycle --height=50% -q "${*}" -1 -0)" && cd "${sel}" && unset sel; ls -FAh --group-directories-first ;; esac ;}
h() { info "${*}" || man "${*}" || help "${*}" || "${*}" --help || /usr/bin/"${*}" --help ;}
t() { [ -d "${HOME}"/.trash ] || mkdir -p "${HOME}"/.trash ; sel="$(du -ha "${PWD}" | sort -hr | fzf --algo=v1 --cycle --height=50% | cut -f2)" && mv -v "$sel" "${HOME}"/.trash/ && unset sel ;}
v() { sel="$(find . | fzf --cycle --height=50% -q "${*}" -1 -0)" && vim "$sel" && unset sel ;} #:w !sudo tee > /dev/null % ###
#m() { f="$(find . | fzf --algo=v1 --cycle --height=50% -1 -0)" && g="$(find .. -type d | fzf --algo=v1 --cycle --height=50% -1 -0)" && mv -i "$f" "$g" ;}
p() { sel="$(find "${PWD}" | fzf --algo=v1 --cycle --height=50% -q "${*}" -1 -0)" && mpv --audio-channels=2 --audio-display=no "$sel" && unset sel ;}
pl() { sel="$(ls -t "${PWD}" | tail -n 1)" && mpv --audio-channels=2 --audio-display=no --fs --speed=1.1 "$sel" && unset sel ;} #awk 'FNR==3{print}'
le() { sel="$(find "${PWD}" -type f | fzf --cycle --height=50% -q "${*}" -1 -0)" && less -Mi "$sel" && unset sel ;}
mcd() { mkdir -p "$1" ; cd "$1" || return ;}
sp() { w3m -o auto_image=TRUE "https://searx.be/search?q='${*}'&categories=images" ;}
s1() { w3m -o auto_image=TRUE "https://searx.be/search?q='${*}'" ;}
s2() { w3m -o auto_image=TRUE "https://searx.prvcy.eu/searxng/search?q='${*}'" ;}
s3() { w3m -o auto_image=TRUE "https://search.ononoki.org/search?q='${*}'" ;}
s4() { w3m -o auto_image=TRUE "https://searx.kutay.dev/search?q='${*}'" ;}
s5() { w3m -o auto_image=TRUE "https://searx.headpat.exchange/search?q='${*}'" ;}
s6() { w3m -o auto_image=TRUE "https://paulgo.io/search?q='${*}'" ;}
s7() { w3m -o auto_image=TRUE "http://searxng.nicfab.eu/searxng/search?q='{*}'" ;}
s8() { w3m -o auto_image=TRUE "https://northboot.xyz/search?q='${*}'" ;}
s9() { w3m -o auto_image=TRUE "https://search.sapti.me/search?q='${*}'" ;}
s10() { w3m -o auto_image=TRUE "https://search.rhscz.eu/search?q='${*}'" ;}
s11() { w3m -o auto_image=TRUE "https://opnxng.com/search?q='${*}'" ;}
s12() { w3m -o auto_image=TRUE "https://search.mdosch.de/search?q='${*}'" ;}
wb() { w3m -o auto_image=TRUE "https://wiby.me/?q='${*}'" ;}
ff() { w3m -o auto_image=TRUE "http://www.frogfind.com/?q='${*}'" ;}
wce() { d="$(TZ=US date +%F)" && lynx -dump -width 170 "https://en.wikipedia.org/wiki/Portal:Current_events" | grep -A 90 -m 1 "$d" | less ;}
ch() { wget -qO- cheat.sh/"${*}" ;}
wka() { w3m -o auto_image=TRUE "https://wiki.archlinux.org/index.php?search='${*}'" ;}
wk() { lynx -dump -width 120 "https://en.wikipedia.org/w/index.php?go=Go&search='${*}'" | grep -i 'search results' -A 50 ;}
#qw() { w3m -o auto_image=TRUE "https://www.qwant.com/?q='${*}'" ;}
#yd() { w3m -o auto_image=TRUE "https://yandex.com/search/?text='${*}'" ;}
#mg() { w3m -o auto_image=TRUE "https://metager.org/meta/meta.ger3?eingabe='${*}'" ;}
#es() { w3m -o auto_image=TRUE "https://www.ecosia.org/search?method=index&q='${*}'" ;}
#gw() { w3m -o auto_image=TRUE "https://search.givewater.com/serp?q='${*}'" ;}
#wa() { w3m -o auto_image=TRUE "https://www.wolframalpha.com/input?i='${*}'" ;}
#br() { w3m -o auto_image=TRUE "https://boardreader.com/s/'${*}'.html;language=English" ;}
#gi() { w3m -o auto_image=TRUE "https://gibiru.com/results.html?q='${*}'" ;}
#lk() { w3m -o auto_image=TRUE "https://www.lukol.com/s.php?q='${*}'" ;}
#gb() { w3m -o auto_image=TRUE "https://www.gigablast.com/search?c=main&qlangcountry=en-us&q='${*}'" ;}
#grd() { lynx "gopher://gopherddit.com/7/cgi-bin/reddit.cgi?${*}" ;}
#gpb() { lynx "gopher://bay.parazy.de:666/7/q.dcgi?${*}" ;}
gwk() { lynx "gopher://gopherpedia.com/7/lookup?${*}" ;}
gpb() { lynx "gopher://bay.parazy.de:666/" ;}
gvs() { lynx "gopher://gopher.floodgap.com/7/v2/vs?${*}" ;}
gfg() { lynx gopher://fld.gp:70 ;}
ghn() { lynx gopher://hngopher.com:70 ;}
glu() { lynx gopher://gopher.leveck.us:70 ;}
#g4c() { lynx gopher://khzae.net:70/1/chan ;}
#gmu() { lynx gopher://mozz.us:70 ;}
#gbr() { lynx gopher://bitreich.org/1 ;}
#gcv() { lynx gopher://codevoid.de:70 ;}
#gopher://gopher.quux.org/
#gopher://telcodata.us:70 #NPA/NXX lookup
#gopher://home.jumpjet.info/
#gopher://dante.pglaf.org:70
#gopher://tilde.club/1/~mz721
#IFS="$(printf '\n\t')"
#(,),[,],.,*,?,+,|,^ and $ are special symbols in BRE, escaped with a backslash to be treated as literal
#strip out control characters and non-UTF-8 characters before display using printf '%s' "$file" | LC_ALL=POSIX tr -d '[:cntrl:]' | iconv -cs -f UTF-8 -t UTF-8
# magic sysrq : alt+prtscr REISUB=reboot REISUO=poweroff
## lvcreate -L 1GB -s -n tecmint_datas_snap /dev/vg_tecmint_extra/tecmint_datas ##creating lvm snapshot
## lvremove /dev/vg_tecmint_extra/tecmint_datas_snap
## lvconvert --merge /dev/vg_tecmint_extra/tecmint_data_snap ##restore snapshot
#
#torsocks w3m piratebayo3klnzokct3wt5yyxb2vpebbuyjl7m623iaxmqhsd52coid.onion ##tpb
#https://www.wonderhowto.com/search/"${*}"
#https://www.shodan.io/search?query="${*}"
#http://wiki47qqn6tey4id7xeqb6l7uj6jueacxlqtk3adshox3zdohvo35vad.onion/  ##hiddenwiki
#gopher://gnr2pc7do3kxgj2r4awdpcg5inaxrqb3tbfii7ubjhkdbzl7xuom5gyd.onion ##khzae.net
#gopher://enlrupgkhuxnvlhsf6lc3fziv5h2hhfrinws65d7roiv6bfj7d652fid.onion/1/ ##bitreich
#gopher://34wdliz62iyj7hlst66rb5zczixobpujwzos5fhssik7kt3thwew7kid.onion/1/ ##leot
#gopher://4kcetb7mo7hj6grozzybxtotsub5bempzo4lirzc3437amof2c2impyd.onion/1 ##lumidity
#gopher://blackgviz2y4nhrd.onion/1 ##tomasino
#gopher://codevoid4p3lowez.onion/1/
#gopher://dek6dax7nbpb3sld.onion/1/ ##THEEND
#gopher://goatsex6p2gn5rqx.onion/1 ##goatsex6p2gn5rqx
#gopher://ibpj4qv7mufde33w.onion/1 ##taz.de
#gopher://j5c635m7dgyytg2l7ql7ulygoaidy2dly62onhd4cvbsbcxxt6xvmcid.onion/1 ##saccophore
#gopher://t22ggazr55vk3cxldrpb7zaftgbth2mx6apwgbba7afrno5mfu2k33yd.onion/1/ ##jhumprey's

ts() { transmission-daemon -b && sleep 0.2 && transmission-remote -l ;}
tk() { pkill transmission-daemon ;}
tl() { printf "\033c"; transmission-remote -l ;}
ta() { transmission-remote -a "${*}" --start ;}
td() { transmission-remote -t "${*}" -r ;}
th() { transmission-remote -a magnet:?xt=urn:btih:"${*}" --start ;}
ti() { transmission-remote -t "${*}" -if ;}
tp() { transmission-remote -t "${*}" -ip ;}
#tg() { transmission-remote -t "${*}" -G all -g all ;} #g-get G=no-get


##extract archive file with appropriate progs
extract() { n="$(find "${PWD}" | fzf --cycle --height=50% -q "${*}" -1 -0)" &&
case "$n" in
  *.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar) tar xvf "$n" ;;
  *.lzma) unlzma "$n"      ;;
  *.bz2)  bunzip2 "$n"     ;;
  *.rar)  unrar x "$n"     ;;
  *.gz)   gunzip "$n"      ;;
  *.zip)  unzip "$n"       ;;
  *.z)    uncompress "$n"  ;;
  *.7z|*.arj|*.cab|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.rpm|*.udf|*.wim|*.xar) 7z x "$n" ;;
  *.xz)   unxz "$n"        ;;
  *.exe)  cabextract "$n"  ;;
  *) echo "extract: '$n' - unknown archive method"; return 1 ;;
esac ;}

#bld='\[\033[01;1m\]'   # Bold
#itl='\[\033[01;3m\]'   # Italic
#udl='\[\033[01;4m\]'   # Underline
#sbl='\[\033[01;5m\]'   # SlowBlink
#blk='\[\033[01;30m\]'   # Black
#red='\[\033[01;31m\]'   # Red
#grn='\[\033[01;32m\]'   # Green
#ylw='\[\033[01;33m\]'   # Yellow
#blu='\[\033[01;34m\]'   # Blue
#pur='\[\033[01;35m\]'   # Purple
#cyn='\[\033[01;36m\]'   # Cyan
#wht='\[\033[01;37m\]'   # White
#clr='\[\033[00m\]'      # Reset
#osc='\033]52;c;'       #osc52

#text editor in browser# data:text/html,<body contenteditable style="line-height:1.5;font-size:20px;">
