#!/data/data/com.termux/files/usr/bin/sh

#mpv-android
termux_player () {
    am start --user 0 -a android.intent.action.VIEW -d "$1" is.xyz.mpv/.MPVActivity
}
url_handler=termux_player

##vlc
#termux_player () {
#    am start -a android.intent.action.VIEW -d "$1" org.videolan.vlc/.StartActivity
#}
#url_handler=termux_player

##yt/newpipe
#termux_player () {
#    termux-open "$1"
#}
#url_handler=termux_player
