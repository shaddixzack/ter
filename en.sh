cd "${HOME}"/storage/shared/videos/ &&

for w in ./*_en[._]* ./*-en[.-]* ./*_id.* ./*-id.*; 
do x=$(printf %sa "${w}" | sed 's/_en_US//g;s/_en_GB//g;s/_en\./\./g;s/-en-US//g;s/-en-GB//g;s/-en-MY//g;s/-en\./\./g;s/_id\./\./g;s/-id\./\./g') 
    mv -v -- "${w}" "${x%a}"
    unset w x
done && cd || exit
