require 'sinatra'
require 'open3'

get '/youtube' do
  erb :youtube
end

get '/youtube/download' do
  command = "yt-dlp -f "
  if params['format'] == "low_video"
    #command += "'best[filesize<17M]' "
    command += "'best[height<480p]' "
  elsif params['format'] == "high_video"
    command += "'best[height<=720]' "
  elsif params['format'] == "audio"
    command += "140 "
  end
  command += "'#{params['url']}'"
  command += " -o './downloads/%(title)s.%(ext)s'"
  file_name_command = command + " --get-filename"
  status = system(command)
  file_name, _, _ = Open3.capture3(file_name_command)
  if status
    redirect to("/download?filename=#{file_name}")
  else
    erb :error
  end
end

get '/download' do
  file_name = params['filename']
  actual_name = file_name.split('/')[-1]
  send_file "#{file_name}", :filename => actual_name, :type => 'Application/octet-stream'
end





require 'sinatra'

get '/pdf' do
  erb :pdf_upload
end

post '/pdf/upload' do
  file_name = params[:file][:filename].gsub(/\s+/, "_")
  file_extension = file_name.split('.')[1]
  if file_extension != "pdf"
    erb :pdf_error
  else
    file = params[:file][:tempfile]
    file_path = "./attachments/"
    File.open(file_path + file_name, 'wb') do |f|
      f.write(file.read)
    end
    output_filename = file_path + file_name.split('.')[0] + "_compressed." + file_extension
    command = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile=#{output_filename} #{file_path + file_name}"
    system(command)
    redirect to("/download?filename=#{output_filename}")
  end
end

get '/download' do
  file_name = params['filename']
  actual_name = file_name.split('/')[-1]
  send_file "#{file_name}", :filename => actual_name, :type => 'Application/octet-stream'
end





require 'sinatra'

get '/fileup' do
  erb :file_upload
end

post '/file/upload' do
  file_name = params[:file][:filename].gsub(/\s+/, "_")
  file = params[:file][:tempfile]
  file_path = "./uploads/" + file_name
  File.write('./uploads/' + params['user'] + '.txt', file_path)
  File.open(file_path, 'wb') do |f|
    f.write(file.read)
  end
  erb :file_upload_success
end

get '/filedown' do
  erb :file_download
end

get '/file/download' do
  file_path = File.read('./uploads/' + params['user'] + '.txt')
  redirect to("/download?filename=#{file_path}")
end

get '/download' do
  file_name = params['filename']
  actual_name = file_name.split('/')[-1]
  send_file "#{file_name}", :filename => actual_name, :type => 'Application/octet-stream'
end




require 'sinatra'

get '/copy' do
  erb :clipboard_copy
end

post '/clipboard/copy' do
  clipboard_path = 'clipboard/' + params['user'] + '.txt'
  File.write(clipboard_path, params['text'])
  erb :clipboard_copy_success
end

get '/paste' do
  erb :clipboard_paste
end

get '/clipboard/paste' do
  clipboard_path = 'clipboard/' + params['user'] + '.txt'
  clipboard_text = File.read(clipboard_path)
  erb :render_text, :locals => {:text => clipboard_text}
end
