#!/data/data/com.termux/files/usr/bin/sh

termux-setup-storage; termux-change-repo;
apt update; yes | apt upgrade; yes | apt install root-repo x11-repo unstable-repo tur-repo 
yes | apt install termux-services termux-exec termux-api
yes | apt install man manpages mandoc shellcheck nyancat asciiquarium rtmpdump ncurses-utils task-spooler 
yes | apt install openssh rtorrent transmission tor torsocks i2pd proxychains-ng tmate mosh tmux zellij shellinabox
yes | apt install zathura zathura-pdf-poppler unoconv docx2txt antiword catdoc wordnet sc-im wordgrinder fbi
yes | apt install htop ncdu jq fzf busybox mc lf nnn barcode qrencode zbar-tools abook calcurse hardinfo yank xwallpaper pwgen git vim 
yes | apt install libcaca chafa 
yes | apt install scrcpy adb fastboot 
yes | apt install python miniserve webfs
yes | apt install 7zip zip unzip unrar cabextract
yes | apt install curl wget axel aria2 rsync w3m lynx socat 
yes | apt install newsboat neomutt isync weechat irssi profanity picocom minimodem morse morsegen
yes | apt install imagemagick cmus cmusfm mpv ffmpeg sox mediainfo vlc gstreamer mpg123 tvheadend minidlna
#yes | apt install bspwm sxhkd polybar rofi

#setup ssh in termux (apt install openssh and passwd then sshd)
#share youtube, and other supported links, to termux, and it automatically downloads with yt-dlp (curl -sL bit.ly/TermuxDL | bash
#calibre-debug --run-plugin EpubMerge -- ${1+"$@"}
#busybox tcpsvd -vE 0.0.0.0 2121 busybox ftpd -w path/to/serve # -w allows upload and only use port above 1024
#busybox httpd -p 0.0.0.0:8080 #HTTP server 
#tinyproxy -d #HTTP proxy server
#SOCKSPort 127.0.0.1:9050 # Enable TOR SOCKS proxy
#HiddenServiceDir /data/data/com.termux/files/home/.tor/hidden_ssh  #Hidden Service: SSH , mkdir -p ~/.tor/hidden_ssh, 
#HiddenServicePort 22 127.0.0.1:8022                                #Hidden Service: SSH, cat ~/.tor/hidden_ssh/hostname 
#https://gitlab.com/shaddixzack/dot/-/raw/main/urls #https://github.com/shaddixzack/dot/raw/main/urls
#https://gitlab.com/shaddixzack.atom?feed_token=xMfMQysSprDbD7_BhR6R
#ffmpeg -f x11grab -s wxga -r 25 -i :0.0 -sameq /tmp/out.mpg ##Capture video using x11
#sudo -b ffmpeg -n -hide_banner -nostats -loglevel panic -f alsa -i "hw:0,0" -f fbdev -r 25 -i /dev/fb0 -c:v libx264 -c:a flac $filename #Capture video using dev/fb0
