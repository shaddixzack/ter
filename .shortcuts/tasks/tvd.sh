#!/data/data/com.termux/files/usr/bin/sh
#
cvol="$(termux-volume | jq ".[] | select(.stream==\"music\").volume")" && termux-volume music "$((cvol-2))" && termux-toast -g top -s "$cvol" && pkill termux-volume 
